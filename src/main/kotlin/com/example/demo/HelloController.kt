package com.example.demo

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class HelloController {
    @GetMapping
    fun hello(
            @RequestParam("name", required = false, defaultValue = "world") name: String
    ) = mapOf("response" to "Hello $name")
}
